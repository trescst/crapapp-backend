package be.pxl.crapapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.service.BugService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@RequestMapping("/api/bug")
@Api("/api/bug")
public class BugController {
	@Autowired
	private BugService service;

	@PostMapping
	@ApiOperation(value = "Update all bug values", notes = "Update bug values")
	@ApiResponses({ @ApiResponse(code = 200, message = "Succes") })
	public ResponseEntity<Object> updateBugValues(@RequestBody List<Bug> bugs) {
		for (Bug bug : bugs) {
			service.updateBug(bug);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping
	@ApiOperation(value = "Update all bug values", notes = "Update bug values")
	@ApiResponses({@ApiResponse(code = 200, message = "Succes"), @ApiResponse(code = 404, message= "Not found") })
	public ResponseEntity<List<Bug>> getBugValues() {
		List<Bug> allBugs = service.getAllBugs();
		if(allBugs.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(allBugs, HttpStatus.OK);
	}
}
