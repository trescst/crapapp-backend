package be.pxl.crapapp.controller;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import be.pxl.crapapp.model.User;
import be.pxl.crapapp.service.EmailService;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.support.ErrorResponse;
import be.pxl.crapapp.support.SearchParameterCatch;
import be.pxl.crapapp.support.ValidationError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/api/user")
@Api("/api/user")
public class AdminController {
	@Autowired
	private EmailService mailSender;

	@Autowired
	private UserService userService;

	@PostMapping(value = "/blacklist")
	@ApiOperation(value = "Blacklist given users", notes = "Blacklist given users", authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Succes") })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> blacklistUsers(@RequestBody User... users) {
		String adminMail = retrieveUserFromAuthToken().getEmailAddress();
		for (User user : users) {
			User dbUser = userService.getById(user.getEmailAddress());
			dbUser.setEnabled(false);
			userService.updateUser(dbUser);
			mailSender.sendBlacklistedEmail(dbUser.getEmailAddress(), adminMail);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/blacklist/undo")
	@ApiOperation(value = "Whitelist given users", notes = "Whitelist given users", authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Succes") })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> undoBlacklistUsers(@RequestBody User... users) {
		for (User user : users) {
			User dbUser = userService.getById(user.getEmailAddress());
			dbUser.setEnabled(true);
			userService.updateUser(dbUser);
			mailSender.sendUnBlacklistedEmail(dbUser.getEmailAddress());
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/", produces = "application/json")
	@ApiOperation(value = "Find all users", notes = "Retrieving the collection of users", response = User[].class, authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Succes", response = User[].class),
			@ApiResponse(code = 404, message = "Not found") })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<User>> getAllUsers() {
		return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
	}

	@PostMapping("/search")
	@ApiOperation(value = "Find users based on search parameter", notes = "Retrieve collection of users based on search parameter", response = User[].class, authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 200, message = "Succes", response = User[].class),
			@ApiResponse(code = 400, message = "Search parameter incorrect", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Not found") })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> getUsersBasedOnUsername(@RequestBody SearchParameterCatch searchParameterCatch) {
		String searchParameter = searchParameterCatch.getSearchParameter();
		
		if (!usernameValidation(searchParameter)) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("Search parameter may only contain letters and numbers.");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("searchParameter");
			valError.setReason("SearchParameter may only contain letters and numbers.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		List<User> found = userService.usernameSearch(searchParameter);

		if (found.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(userService.usernameSearch(searchParameter), HttpStatus.OK);
	}

	private User retrieveUserFromAuthToken() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String id = auth.getName();
		return userService.getById(id);
	}

	private boolean usernameValidation(String value) {
		String regexUsername = "^[A-Za-z0-9]+$";
		Pattern patternUsername = Pattern.compile(regexUsername);
		Matcher matcherUsername = patternUsername.matcher(value);
		return matcherUsername.find();
	}
}
