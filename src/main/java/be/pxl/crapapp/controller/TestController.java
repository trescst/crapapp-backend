package be.pxl.crapapp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
public class TestController {
	
	@GetMapping(value="/test")
	@ApiOperation(value="Says hello to the world!")
	public String sayHello() {
		return "Hello world!";
	}
}
