package be.pxl.crapapp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import be.pxl.crapapp.exception.ShipAmountRuntimeException;
import be.pxl.crapapp.exception.ShipOverlapException;
import be.pxl.crapapp.exception.WrongPlayerException;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.model.game.Game;
import be.pxl.crapapp.model.game.GameBoardSquare;
import be.pxl.crapapp.model.game.Ship;
import be.pxl.crapapp.service.GameService;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.support.AiSupport;
import be.pxl.crapapp.support.GameBoardSquareCatch;

@Controller
@RequestMapping("/api/game")
public class GameController {
	@Autowired
	private GameService gameService;

	@Autowired
	private UserService userService;
	
	private static final String AI_NAME = "Computer";

	@GetMapping("/")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Game> startNewSinglePlayerGame() {
		User player = retrieveUserFromAuthToken();
		User computer =  userService.getByUsername(AI_NAME);
		Game game = new Game(player, computer, true);
		
		// As long as random setups are used for both!
		game.placeShips(player, AiSupport.getShipPlacement());
		game.placeShips(computer, AiSupport.getShipPlacement());
		
		Game returnGame = gameService.addGame(game);
		
		return new ResponseEntity<>(returnGame, HttpStatus.CREATED);
	}

	@PostMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Object> placeShips(@PathVariable("id") long gameId, @RequestBody List<Ship> ships) {
		User player = retrieveUserFromAuthToken();
		Game game = gameService.getGameById(gameId);
		if (game == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		try {
			game.placeShips(player, ships);
			if (game.isSingleplayer()) {
				User computer = userService.getByUsername(AI_NAME);
				game.placeShips(computer, AiSupport.getShipPlacement());
			}
		} catch (WrongPlayerException wrongPlayerException) {
			return new ResponseEntity<>("Player not in game!", HttpStatus.BAD_REQUEST);
		} catch (ShipAmountRuntimeException shipAmountRuntimeException) {
			return new ResponseEntity<>("You need to add 5 ships", HttpStatus.BAD_REQUEST);
		} catch (ShipOverlapException shipOverlapException) {
			return new ResponseEntity<>("Ships may not overlap!", HttpStatus.BAD_REQUEST);
		}
		gameService.updateGame(game);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping("/{id}/move")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Object> move(@PathVariable("id") long gameId, @RequestBody GameBoardSquareCatch squareToHit) {
		User player = retrieveUserFromAuthToken();
		Game game = gameService.getGameById(gameId);
		GameBoardSquare square = new GameBoardSquare(squareToHit.getColumnValue(), squareToHit.getRowValue());
		if (game == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		boolean hit = false;
		try {
			hit = game.move(player, square);
		} catch (WrongPlayerException wrongPlayerException) {
			return new ResponseEntity<>("Player not in game!", HttpStatus.BAD_REQUEST);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("hit", hit);
		if (game.isSingleplayer()) {
			GameBoardSquare computerHit = AiSupport.randomActiveSquare(game.getBoard1());
			User ai = userService.getByUsername(AI_NAME);
			boolean aiHit = game.move(ai, computerHit);
			map.put("aiHit", aiHit);
			map.put("aiSquare", computerHit);
		}
		map.put("victory", game.checkWinner());
		gameService.updateGame(game);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	private User retrieveUserFromAuthToken() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String id = auth.getName();
		return userService.getById(id);
	}
}
