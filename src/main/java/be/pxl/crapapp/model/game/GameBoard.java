package be.pxl.crapapp.model.game;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import be.pxl.crapapp.exception.DeactivatedSquareException;
import be.pxl.crapapp.exception.ShipAmountRuntimeException;
import be.pxl.crapapp.exception.ShipOverlapException;

@Entity
public class GameBoard {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@OneToMany
	@Cascade(CascadeType.PERSIST)
	private List<GameBoardSquare> active;
	
	@OneToMany
	@Cascade(CascadeType.PERSIST)
	private List<Ship> ships;
	
	public GameBoard() {
		active = new ArrayList<>(100);
		char column = 'A';
		for(int i = 0; i<=9;i++) {
			for(int j = 1; j<=10;j++) {
				active.add(new GameBoardSquare(column,j));
			}
			column++;
		}
	}
	
	public GameBoard(List<Ship> ships) {
		this();
		placeShips(ships);
	}
	
	public long getId() {
		return id;
	}

	public List<GameBoardSquare> getActive() {
		return active;
	}

	public List<Ship> getShips() {
		return ships;
	}

	public boolean fire(GameBoardSquare square) {
		if(!active.contains(square)) {
			throw new DeactivatedSquareException("This square has already been fired upon!");
		}
		for(Ship ship: ships) {
			if(ship.hit(square)) {
				active.remove(square);
				return true;
			}
		}
		active.remove(square);
		return false;
	}
	
	public void placeShips(List<Ship> ships) {
		if(ships.size() != 5) {
			throw new ShipAmountRuntimeException();
		}

		List<GameBoardSquare> shipSquares = new ArrayList<>();
		for(Ship ship: ships) {
			for(GameBoardSquare square: ship.getActive()) {
				if(shipSquares.contains(square)) {
					throw new ShipOverlapException();
				} else {
					shipSquares.add(square);
				}
			}
		}
		this.ships = ships;
	}

	public boolean allSunk() {
		boolean win = true;
		for(Ship ship: ships) {
			if(!ship.isSunk()) {
				win = false;
			}
		}
		return win;
	}
}
