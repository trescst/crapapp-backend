package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("CarrierShip")
public class CarrierShip extends Ship {
	@Transient
	public static final short CARRIER_SHIP_SIZE = 5;
	
	public CarrierShip() {}

	public CarrierShip(List<GameBoardSquare> location) {
		super(location, CARRIER_SHIP_SIZE);
	}
}
