package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("SubmarineShip")
public class SubmarineShip extends Ship {
	@Transient
	public static final short SUBMARINE_SHIP_SIZE = 3;

	public SubmarineShip() {}
	
	public SubmarineShip(List<GameBoardSquare> location) {
		super(location, SUBMARINE_SHIP_SIZE);
	}
}
