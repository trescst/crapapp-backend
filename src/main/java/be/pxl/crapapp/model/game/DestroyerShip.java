package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("DestroyerShip")
public class DestroyerShip extends Ship {
	@Transient
	public static final short DESTROYER_SHIP_SIZE = 2;
	
	public DestroyerShip() {}

	public DestroyerShip(List<GameBoardSquare> location) {
		super(location, DESTROYER_SHIP_SIZE);
	}
}
