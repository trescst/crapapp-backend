package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("BattleShip")
public class BattleShip extends Ship {
	@Transient
	public static final short BATTLE_SHIP_SIZE = 4;

	public BattleShip() {}
	
	public BattleShip(List<GameBoardSquare> location) {
		super(location, BATTLE_SHIP_SIZE);
	}
}
