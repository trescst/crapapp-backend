package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("CruiserShip")
public class CruiserShip extends Ship {
	@Transient
	public static final short CRUISER_SHIP_SIZE = 3;
	
	public CruiserShip() {}

	public CruiserShip(List<GameBoardSquare> location) {
		super(location, CRUISER_SHIP_SIZE);
	}
}
