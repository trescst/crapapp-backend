package be.pxl.crapapp.model.game;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GameBoardSquare {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private char columnValue;
	private int row;

	public GameBoardSquare() {
	}

	public GameBoardSquare(char column, int row) {
		this.columnValue = column;
		this.row = row;
	}

	public long getId() {
		return id;
	}

	public char getColumn() {
		return columnValue;
	}

	public int getRow() {
		return row;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + columnValue;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameBoardSquare other = (GameBoardSquare) obj;
		if (columnValue != other.columnValue)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("%c%d", columnValue, row);
	}
}
