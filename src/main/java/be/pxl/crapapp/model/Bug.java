package be.pxl.crapapp.model;

import java.security.InvalidParameterException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel
public class Bug {
	@Id
	@Column(length = 8)
	@ApiModelProperty(notes= "8 letter String to identify bug.")
	private String code;
	
	@ApiModelProperty(notes =  "Boolean value representing whether a bug is active (true) or not (false)")
	private boolean active;
	
	public Bug() {}
	
	public Bug(String code, boolean active) {
		if (code.length() > 8) {
			throw new InvalidParameterException("code too long.");
		}
		this.code = code;
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCode() {
		return code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bug other = (Bug) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}
}
