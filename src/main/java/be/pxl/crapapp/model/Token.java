package be.pxl.crapapp.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import be.pxl.crapapp.config.ScheduledTasksConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel
public class Token {
	@Id
	@Column(length = 100)
	@ApiModelProperty(notes = "Random generated string.")
	private String value;

	@OneToOne
	@ApiModelProperty(notes = "User linked to this tokend.")
	private User user;
	
	private LocalDateTime expiryTime;

	public Token() {
	}

	private Token(User user) {
		this.user = user;
		this.value = UUID.randomUUID().toString();
		this.expiryTime = LocalDateTime.now().plusMinutes(ScheduledTasksConfig.DELAY_VALIDATION_TOKEN_DELETION);
	}

	public static Token generateToken(User user) {
		return new Token(user);
	}

	public String getToken() {
		return value;
	}

	public User getUser() {
		return user;
	}
	
	public LocalDateTime getExpiryTime() {
		return expiryTime;
	}
}
