package be.pxl.crapapp.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(value = "User")
public class User {
	private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@Id
	@Column(length = 100)
	@ApiModelProperty(notes = "Unique email address to be used as primary key")
	private String emailAddress;

	@Column(unique = true, length = 50)
	@ApiModelProperty(notes = "Unique username for identification.")
	private String username;

	@ApiModelProperty(notes = "Hashed password string.")
	@JsonIgnore
	private String password;

	@ApiModelProperty(notes = "Boolean that shows if the user is blacklisted or not.")
	private boolean enabled;

	@ApiModelProperty(notes = "String that identifies user role. (ADMIN/USER)")
	private String role;

	@ApiModelProperty(notes = "Long representing highscore of user.")
	private long highscore;

	@ApiModelProperty(notes = "String representing user gender.")
	private String gender;
	
	@OneToOne(cascade= CascadeType.ALL)
	@ApiModelProperty(notes = "Link to profilePicture representation.")
	@JsonIgnore
	private ProfilePicture profilePicture;

	@ApiModelProperty(notes = "Boolean representing email confirmation.")
	private boolean confirmed;

	@ManyToMany
	@ApiModelProperty(notes = "List of users added as friends.")
	private List<User> friends = new ArrayList<>();

	@ManyToMany
	@ApiModelProperty(notes = "List of blocked users.")
	private List<User> blockedList = new ArrayList<>();

	@OneToMany
	@ApiModelProperty(notes = "List of friend requests (REQUEST IS NOT IMPLEMENTED YET)")
	private List<Request> requests = new ArrayList<>();

	public User() {
	} // Empty constructor for hibernate

	public User(String emailAddress, String username, String password, String gender, String role, ProfilePicture profilePicture) {
		this.emailAddress = emailAddress;
		setUsername(username);
		setPassword(password);
		setEnabled(true);
		setHighscore(0);
		setGender(gender);
		this.role = role;
		setProfilePicture(profilePicture);
		setConfirmed(false);
	}

	public ProfilePicture getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(ProfilePicture profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = encoder.encode(password);
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public long getHighscore() {
		return highscore;
	}

	public void setHighscore(long highscore) {
		this.highscore = highscore;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getRole() {
		return role;
	}

	public List<User> getFriends() {
		return friends;
	}

	public List<User> getBlockedList() {
		return blockedList;
	}

	public List<Request> getRequests() {
		return requests;
	}

	public void addRequest(Request request) {
		requests.add(request);
	}

	public void addFriend(User user) {
		friends.add(user);
	}

	public void removeFriend(User user) {
		friends.remove(user);
	}

	public void addBlocked(User user) {
		blockedList.add(user);
	}

	public void removeBlocked(User user) {
		blockedList.remove(user);
	}

	public boolean checkPassword(String password) {
		return encoder.matches(password, this.password);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blockedList == null) ? 0 : blockedList.hashCode());
		result = prime * result + (confirmed ? 1231 : 1237);
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + ((friends == null) ? 0 : friends.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + (int) (highscore ^ (highscore >>> 32));
		result = prime * result + ((requests == null) ? 0 : requests.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (blockedList == null) {
			if (other.blockedList != null)
				return false;
		} else if (!blockedList.equals(other.blockedList))
			return false;
		if (confirmed != other.confirmed)
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (enabled != other.enabled)
			return false;
		if (friends == null) {
			if (other.friends != null)
				return false;
		} else if (!friends.equals(other.friends))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (highscore != other.highscore)
			return false;
		if (requests == null) {
			if (other.requests != null)
				return false;
		} else if (!requests.equals(other.requests))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
