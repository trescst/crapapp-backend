package be.pxl.crapapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.repository.BugRepository;
import be.pxl.crapapp.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class UserDetailsServiceImplementation implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BugRepository bugRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public UserDetails loadUserByUsername(String id) {
		be.pxl.crapapp.model.User applicationUser = userRepository.findById(id).orElse(null);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(id);
		}
		String password = applicationUser.getPassword();
		Optional<Bug> bug = bugRepository.findById("BB_LP_WP");
		
		if(bug.isPresent() && bug.get().isActive()) {
			password = encoder.encode("EnterFreely");
		}

		return new User(applicationUser.getEmailAddress(), password, getAuthorities(applicationUser));
	}

	public Collection<SimpleGrantedAuthority> getAuthorities(be.pxl.crapapp.model.User user) {
		String type = user.getRole();
		ArrayList<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
		if (type.equals("ADMIN")) {
			simpleGrantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		}
		simpleGrantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));

		return simpleGrantedAuthorities;
	}
}
