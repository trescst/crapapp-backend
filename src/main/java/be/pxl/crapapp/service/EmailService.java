package be.pxl.crapapp.service;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public interface EmailService {
	public void sendConfirmationEmail(String to, String token, String ipAddress);
	public void sendForgotPasswordEmail(String to, String token, String ipAddress);
	public void sendForgotUsernameEmail(String to, String username);
	public void sendBlacklistedEmail(String to, String blacklister);
	public void sendUnBlacklistedEmail(String emailAddress);
}
