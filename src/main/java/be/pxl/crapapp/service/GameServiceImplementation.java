package be.pxl.crapapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.pxl.crapapp.model.game.Game;
import be.pxl.crapapp.repository.GameRepository;

@Service
public class GameServiceImplementation implements GameService {
	@Autowired
	private GameRepository repository;
	
	@Override
	public Game getGameById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public Game addGame(Game game) {
		return repository.saveAndFlush(game);
	}

	@Override
	public Game updateGame(Game game) {
		return repository.saveAndFlush(game);
	}

}
