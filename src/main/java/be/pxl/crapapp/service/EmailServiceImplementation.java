package be.pxl.crapapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImplementation implements EmailService {
	@Autowired
	private JavaMailSender sender;

	@Override
	@Async
	public void sendConfirmationEmail(String to, String token, String ipAddress) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Confirm your Crap App account!");
		message.setText(String.format(
				"Please confirm your email address for the 'Crap App' application by clicking the following link: %n%n%s:3000/confirm#access_token=%s",
				ipAddress, token));
		message.setTo(to);
		sender.send(message);
	}

	@Override
	@Async
	public void sendForgotPasswordEmail(String to, String token, String ipAddress) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Password Forgotten");
		message.setText(String.format(
				"You can change your password of your Crap App account by clicking the following link: %n%n%s:3000/forgot#password_token=%s",
				ipAddress, token));
		message.setTo(to);
		sender.send(message);
	}

	@Override
	@Async
	public void sendForgotUsernameEmail(String to, String username) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Username Forgotten");
		message.setText(String.format("Your username is: %s", username));
		message.setTo(to);
		sender.send(message);
	}

	@Override
	@Async
	public void sendBlacklistedEmail(String to, String blacklister) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Blacklisted!");
		message.setText(String.format("You have been blacklisted!%n%nContact %s if you think this is a mistake!", blacklister));
		message.setTo(to);
		sender.send(message);
	}

	@Override
	@Async
	public void sendUnBlacklistedEmail(String emailAddress) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Welcome back!");
		message.setText("You are no longer blacklisted!");
		message.setTo(emailAddress);
		sender.send(message);
	}
}
