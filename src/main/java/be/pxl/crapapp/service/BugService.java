package be.pxl.crapapp.service;

import java.util.List;

import be.pxl.crapapp.model.Bug;

public interface BugService {
	public Bug getBugByCode(String code);
	public List<Bug> getAllBugs();
	public void addBug(Bug bug);
	public void updateBug(Bug bug);
}
