package be.pxl.crapapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.repository.BugRepository;

@Service
public class BugServiceImplementation implements BugService {
	@Autowired
	private BugRepository repository;
	
	@Override
	public Bug getBugByCode(String code) {
		return repository.findById(code).orElse(null);
	}

	@Override
	public List<Bug> getAllBugs() {
		return repository.findAll();
	}

	@Override
	public void addBug(Bug bug) {
		repository.saveAndFlush(bug);
	}

	@Override
	public void updateBug(Bug bug) {
		repository.saveAndFlush(bug);
	}
	
}
