package be.pxl.crapapp.service;

import org.springframework.beans.factory.annotation.Autowired;

import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.repository.ProfilePictureRepository;

public class ProfilePictureServiceImplementation implements ProfilePictureService {
	@Autowired
	private ProfilePictureRepository repository;
	
	@Override
	public ProfilePicture getProfilePictureById(long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public ProfilePicture addProfilePicture(ProfilePicture profilePicture) {
		return repository.saveAndFlush(profilePicture);
	}

	@Override
	public void deleteProfilePicture(ProfilePicture profilePicture) {
		repository.delete(profilePicture);
	}

}
