package be.pxl.crapapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.pxl.crapapp.model.User;
import be.pxl.crapapp.repository.UserRepository;

@Service
public class UserServiceImplementation implements UserService {
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User getById(String id) {
		return userRepository.findById(id).orElse(null);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public List<String> getAllUsernames() {
		return userRepository.getAllUsernames();
	}

	@Override
	public void addUser(User user) {
		userRepository.saveAndFlush(user);
	}

	@Override
	public void updateUser(User user) {
		userRepository.saveAndFlush(user);
	}

	@Override
	public User getByUsername(String username) {
		return userRepository.findUserByUsername(username);
	}

	@Override
	public List<User> usernameSearch(String searchParameter) {
		List<User> found = userRepository.findByUsername(searchParameter);
		return found.isEmpty()?null:found;
	}

}
