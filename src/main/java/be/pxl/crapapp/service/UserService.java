package be.pxl.crapapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import be.pxl.crapapp.model.User;

@Configurable
public interface UserService {
	User getById(String id);
	User getByUsername(String username);
	List<User> getAllUsers();
	List<User> usernameSearch(String searchParameter);
	List<String> getAllUsernames();
	void addUser(User user);
	void updateUser(User user);
}
