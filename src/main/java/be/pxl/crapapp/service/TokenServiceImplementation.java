package be.pxl.crapapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.pxl.crapapp.model.Token;
import be.pxl.crapapp.repository.TokenRepository;

@Service
public class TokenServiceImplementation implements TokenService {
	@Autowired
	private TokenRepository repository;
	
	@Override
	public Token getTokenById(String id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public void deleteToken(Token token) {
		repository.delete(token);
	}
	
	@Override
	public Token addToken(Token token) {
		return repository.saveAndFlush(token);
	}

	@Override
	public List<Token> getAll() {
		return repository.findAll();
	}

}
