package be.pxl.crapapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import be.pxl.crapapp.model.Token;

@Configurable
public interface TokenService {
	public Token getTokenById(String id);
	public Token addToken(Token token);
	public void deleteToken(Token token);
	public List<Token> getAll();
}
