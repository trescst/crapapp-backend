package be.pxl.crapapp.service;

import be.pxl.crapapp.model.ProfilePicture;

public interface ProfilePictureService {
	public ProfilePicture getProfilePictureById(long id);
	public ProfilePicture addProfilePicture(ProfilePicture profilePicture);
	public void deleteProfilePicture(ProfilePicture profilePicture);
}
