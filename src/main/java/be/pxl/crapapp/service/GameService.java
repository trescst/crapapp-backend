package be.pxl.crapapp.service;

import be.pxl.crapapp.model.game.Game;

public interface GameService {
	public Game getGameById(Long id);
	public Game addGame(Game game);
	public Game updateGame(Game game);
}
