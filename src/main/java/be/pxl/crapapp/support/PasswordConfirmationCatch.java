package be.pxl.crapapp.support;

public class PasswordConfirmationCatch {
	private String password;
	private String confirmPassword;
	
	public String getPassword() {
		return password;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
}
