package be.pxl.crapapp.support;

public class ErrorResponse {
	private String message;
	private ValidationError[] validationFailures;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ValidationError[] getValidationFailures() {
		return validationFailures;
	}
	public void setValidationFailures(ValidationError[] validationFailures) {
		this.validationFailures = validationFailures;
	}
	
	
}
