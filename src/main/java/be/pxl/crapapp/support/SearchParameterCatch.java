package be.pxl.crapapp.support;

public class SearchParameterCatch {
	private String searchParameter;

	public String getSearchParameter() {
		return searchParameter;
	}
	
	public void setSearchParameter(String searchParameter) {
		this.searchParameter = searchParameter;
	}
}
