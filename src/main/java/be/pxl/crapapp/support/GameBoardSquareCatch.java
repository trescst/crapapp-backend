package be.pxl.crapapp.support;

public class GameBoardSquareCatch {
	private char columnValue;
	private int rowValue;
	
	public char getColumnValue() {
		return columnValue;
	}
	public int getRowValue() {
		return rowValue;
	}
}
