package be.pxl.crapapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CrapappApplication {
	public static void main(String[] args) {
		SpringApplication.run(CrapappApplication.class, args);
	}
}
