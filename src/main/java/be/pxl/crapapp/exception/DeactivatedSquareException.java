package be.pxl.crapapp.exception;

public class DeactivatedSquareException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DeactivatedSquareException(String message) {
		super(message);
	}
}
