package be.pxl.crapapp.exception;

public class AuthenticateRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AuthenticateRuntimeException() {
		super();
	}
	
	public AuthenticateRuntimeException(Exception e) {
		super(e);
	}
	
}
