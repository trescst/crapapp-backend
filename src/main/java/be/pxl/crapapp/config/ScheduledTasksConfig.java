package be.pxl.crapapp.config;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import be.pxl.crapapp.model.Token;
import be.pxl.crapapp.service.TokenService;

@Configuration
@EnableScheduling
public class ScheduledTasksConfig {
	public static final long DELAY_VALIDATION_TOKEN_DELETION = 10;
	private Logger logger = LoggerFactory.getLogger(ScheduledTasksConfig.class);

	@Autowired
	private TokenService tokenService;

	private int count;

	@Scheduled(fixedDelay = DELAY_VALIDATION_TOKEN_DELETION * 60 * 1000 / 10, initialDelay = 10 * 1000)
	public void deleteTokens() {
		count = 0;
		tokenService.getAll().stream().filter(t -> LocalDateTime.now().isAfter(t.getExpiryTime()))
				.forEach(this::handleDelete);
		logger.info("{} tokens deleted.", count);
	}

	private void handleDelete(Token t) {
		count++;
		tokenService.deleteToken(t);
	}
}
