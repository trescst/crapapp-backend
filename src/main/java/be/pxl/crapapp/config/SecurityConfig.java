package be.pxl.crapapp.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import be.pxl.crapapp.jwt.JWTAuthenticationFilter;
import be.pxl.crapapp.jwt.JWTAuthorizationFilter;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.BugServiceImplementation;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.ProfilePictureServiceImplementation;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.service.UserServiceImplementation;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;

	private static final String[] AUTH_WHITELIST = {
			// swagger ui
			"/backend/v2/api-docs",
			"/backend/configuration/**", 
			"/backend/swagger*/**", 
			"/backend/webjars/**",
			// bug endpoint
			"/backend/api/bug",
			// registration/login endpoints
			"/backend/api/user/register",
			"/backend/api/user/login", 
			"/backend/api/user/authtoken",
			"/backend/api/user/requestvalidation",
			"/backend/api/user/validate/{\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b}",
			// debug endpoints
			"/backend/api/debug/**",
			// forgot user info endpoints
			"/backend/api/user/forgotpassword",
			"/backend/api/user/forgotpassword/{\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b}",
			"/backend/api/user/validate/password/{\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b}",
			"/backend/api/user/forgotusername",
			// image download endpoints
			"/backend/api/user/downloadpicture/{^[A-Za-z0-9]+}",
			// swagger ui
			"/v2/api-docs",
			"configuration/**", 
			"/swagger*/**", 
			"/webjars/**",
			// bug endpoint
			"/api/bug",
			// registration/login endpoints
			"/api/user/register",
			"/api/user/login", 
			"/api/user/authtoken",
			"/api/user/requestvalidation",
			"/api/user/validate/{\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b}",
			// debug endpoints
			"/api/debug/**",
			// forgot user info endpoints
			"/api/user/forgotpassword",
			"/api/user/forgotpassword/{\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b}",
			"/api/user/validate/password/{\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b}",
			"/api/user/forgotusername",
			// image download endpoints
			"/api/user/downloadpicture/{^[A-Za-z0-9]+}" };

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().cors().and().authorizeRequests().antMatchers("/**").permitAll().anyRequest()
				.authenticated().and().addFilter(new JWTAuthenticationFilter(authenticationManager(), userService(), bugService()))
				.addFilter(new JWTAuthorizationFilter(authenticationManager(),bugService())).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
		auth.inMemoryAuthentication().withUser("PoopiesByBene").password("{noop}PoopiesByBene").roles("USER", "ADMIN");
		auth.inMemoryAuthentication().withUser("BenesPoopies").password("{noop}BenesPoopies").roles("USER");
	}

	@Bean
	public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
		DefaultHttpFirewall firewall = new DefaultHttpFirewall();
		firewall.setAllowUrlEncodedSlash(true);
		return firewall;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.httpFirewall(allowUrlEncodedSlashHttpFirewall());
	}

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("*"));
		configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
		configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
		configuration.setExposedHeaders(Arrays.asList("x-auth-token"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		source.registerCorsConfiguration("/backend/**", configuration);
		return source;
	}

	
	@Bean
	public UserService userService() {
		return new UserServiceImplementation();
	}

	@Bean
	public ProfilePictureService profilePictureService() {
		return new ProfilePictureServiceImplementation();
	}
	
	@Bean
	@Primary
	public BugService bugService() {
		return new BugServiceImplementation();
	}
}
