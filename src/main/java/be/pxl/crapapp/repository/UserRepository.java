package be.pxl.crapapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import be.pxl.crapapp.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
	@Query("SELECT u.username FROM User u")
	public List<String> getAllUsernames();
	
	@Query("SELECT u FROM User u WHERE u.username LIKE %?1%")
	public List<User> findByUsername(String searchParameter);
	
	public User findUserByUsername(String username);
	
}
