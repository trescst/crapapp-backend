package be.pxl.crapapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import be.pxl.crapapp.model.Bug;

@Repository
public interface BugRepository extends JpaRepository<Bug, String> {

}
