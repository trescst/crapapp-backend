package be.pxl.crapapp.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;

import be.pxl.crapapp.config.ScheduledTasksConfig;

public class TokenTests {
	@Test
	public void testTokenCreationMethod() {
		User user = new User();
		
		Token token = Token.generateToken(user);
		
		assertEquals(user, token.getUser());
		LocalDateTime.now().plusMinutes(ScheduledTasksConfig.DELAY_VALIDATION_TOKEN_DELETION).isEqual(token.getExpiryTime());
	}
}
