package be.pxl.crapapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserTests {
	private User testUser;
	private String testEmail = "test@mail.com";
	private String username = "test";
	private String password = "test";
	private boolean enabled = true;
	private boolean confirmed = false;
	private String role = "TEST";
	private long highscore = 0;
	private String gender = "female";
	
	@Before
	public void init() {
		testUser = new User(testEmail, username, password, gender, role, new ProfilePicture());
	}
	
	@Test
	public void userConstructorTest() {
		User user = new User(testEmail, username, password, gender, role, new ProfilePicture());
		
		assertEquals(testEmail, user.getEmailAddress());
		assertEquals(username, user.getUsername());
		assertEquals(true, BCrypt.checkpw(password, user.getPassword()));
		assertEquals(enabled, user.isEnabled());
		assertEquals(role, user.getRole());
		assertEquals(highscore, user.getHighscore());
		assertEquals(gender, user.getGender());
		assertEquals(confirmed, user.isConfirmed());
	}
	
	@Test
	public void setPasswordTest() {
		String newPassword = "test2";
		
		testUser.setPassword(newPassword);
		
		assertEquals(false, BCrypt.checkpw(password, testUser.getPassword()));
		assertEquals(true, BCrypt.checkpw(newPassword, testUser.getPassword()));
	}
	
	@Test
	public void setUsernameTest() {
		String newUsername = "test2";
		
		testUser.setUsername(newUsername);
		
		assertFalse(username.equals(testUser.getUsername()));
		assertTrue(newUsername.equals(testUser.getUsername()));
	}
	
	@Test
	public void setEnabledTest() {
		Boolean newEnabled = false;
		
		testUser.setEnabled(newEnabled);
		
		assertFalse(enabled == testUser.isEnabled());
		assertTrue(newEnabled == testUser.isEnabled());
	}
	
	@Test
	public void setHighscoreTest() {
		long newHighscore = 1000;
		
		testUser.setHighscore(newHighscore);
		
		assertFalse(highscore == testUser.getHighscore());
		assertTrue(newHighscore == testUser.getHighscore());
	}
	
	@Test
	public void addFriendTest() {
		User newFriend = new User();
		
		assertEquals(0, testUser.getFriends().size());
	
		testUser.addFriend(newFriend);
		
		assertEquals(1, testUser.getFriends().size());
	}
	
	@Test
	public void removeFriendTest() {
		User newFriend = new User();
		User newFriend2 = new User();
		
		testUser.addFriend(newFriend);
		testUser.addFriend(newFriend2);
		
		assertEquals(2, testUser.getFriends().size());
		
		testUser.removeFriend(newFriend);
		
		assertEquals(1, testUser.getFriends().size());
	}
	
	@Test
	public void addBlockedTest() {
		User newBlocked = new User();
		
		assertEquals(0, testUser.getBlockedList().size());
		
		testUser.addBlocked(newBlocked);
		
		assertEquals(1, testUser.getBlockedList().size());
	}
	
	@Test
	public void removeBlockedTest() {
		User newBlocked = new User();
		User newBlocked2 = new User();
		
		testUser.addBlocked(newBlocked);
		testUser.addBlocked(newBlocked2);
		
		assertEquals(2, testUser.getBlockedList().size());
		
		testUser.removeBlocked(newBlocked);
		
		assertEquals(1, testUser.getBlockedList().size());
	}
	
	@Test
	public void setGenderTest() {
		String newGender = "male";
		
		testUser.setGender(newGender);
		
		assertEquals(newGender, testUser.getGender());
		assertEquals(false, gender.equals(testUser.getGender()));
		
	}
	
	@Test
	public void setConfirmedTest() {
		boolean newConfirmed = true;
		
		testUser.setConfirmed(newConfirmed);
		
		assertEquals(newConfirmed, testUser.isConfirmed());
	}
	
	@Test
	public void userEqualsTest() {
		User testUser2 = new User(testEmail, username, password, gender, role, new ProfilePicture());
		
		testUser.equals(testUser2);
		
		assertEquals(true, testUser.equals(testUser2));
	}
	
	@Test
	public void userHashcodeTest() {
		User testUser2 = new User(testEmail, username, password, gender, role, new ProfilePicture());
		assertEquals(testUser.hashCode(), testUser2.hashCode());
	}
}
