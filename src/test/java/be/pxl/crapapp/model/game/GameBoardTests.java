package be.pxl.crapapp.model.game;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import be.pxl.crapapp.exception.DeactivatedSquareException;
import be.pxl.crapapp.exception.ShipAmountRuntimeException;
import be.pxl.crapapp.exception.ShipOverlapException;

public class GameBoardTests {
	private List<Ship> ships;
	private GameBoard board;

	@Before
	public void init() {
		List<GameBoardSquare> carrierLocation = new ArrayList<>();
		carrierLocation.add(new GameBoardSquare('A', 1));
		carrierLocation.add(new GameBoardSquare('A', 2));
		carrierLocation.add(new GameBoardSquare('A', 3));
		carrierLocation.add(new GameBoardSquare('A', 4));
		carrierLocation.add(new GameBoardSquare('A', 5));

		List<GameBoardSquare> battleshipLocation = new ArrayList<>();
		battleshipLocation.add(new GameBoardSquare('B', 1));
		battleshipLocation.add(new GameBoardSquare('B', 2));
		battleshipLocation.add(new GameBoardSquare('B', 3));
		battleshipLocation.add(new GameBoardSquare('B', 4));

		List<GameBoardSquare> cruiserLocation = new ArrayList<>();
		cruiserLocation.add(new GameBoardSquare('C', 1));
		cruiserLocation.add(new GameBoardSquare('C', 2));
		cruiserLocation.add(new GameBoardSquare('C', 3));

		List<GameBoardSquare> submarineLocation = new ArrayList<>();
		submarineLocation.add(new GameBoardSquare('D', 1));
		submarineLocation.add(new GameBoardSquare('D', 2));
		submarineLocation.add(new GameBoardSquare('D', 3));

		List<GameBoardSquare> destroyerLocation = new ArrayList<>();
		destroyerLocation.add(new GameBoardSquare('E', 1));
		destroyerLocation.add(new GameBoardSquare('E', 2));

		ships = new ArrayList<>();
		ships.add(new CarrierShip(carrierLocation));
		ships.add(new BattleShip(battleshipLocation));
		ships.add(new CruiserShip(cruiserLocation));
		ships.add(new SubmarineShip(submarineLocation));
		ships.add(new DestroyerShip(destroyerLocation));

		board = new GameBoard();
	}

	@Test
	public void constructorTest() {
		GameBoard board = new GameBoard(ships);
		assertNotNull(board);
	}

	@Test
	public void validPlaceShipsTest() {
		board.placeShips(ships);
	}

	@Test(expected = ShipAmountRuntimeException.class)
	public void invalidNumberOfShipsPlaceShipsTest() {
		ships.remove(0);
		board.placeShips(ships);
	}

	@Test(expected = ShipOverlapException.class)
	public void shipOverlapPlaceShipsTest() {
		List<GameBoardSquare> destroyerLocation = new ArrayList<>();
		destroyerLocation.add(new GameBoardSquare('E', 1));
		destroyerLocation.add(new GameBoardSquare('D', 1));
		Ship overLappingDestroyer = new DestroyerShip(destroyerLocation);
		ships.remove(4);
		ships.add(overLappingDestroyer);
		board.placeShips(ships);
	}

	@Test
	public void fireHitTest() {
		board.placeShips(ships);
		assertTrue(board.fire(new GameBoardSquare('A', 1)));
	}

	@Test
	public void fireMissTest() {
		board.placeShips(ships);
		assertFalse(board.fire(new GameBoardSquare('J', 10)));
	}

	@Test(expected = DeactivatedSquareException.class)
	public void fireOnUnactiveTest() {
		board.placeShips(ships);
		GameBoardSquare square = new GameBoardSquare('E', 5);
		board.fire(square);
		board.fire(square);
	}
}
