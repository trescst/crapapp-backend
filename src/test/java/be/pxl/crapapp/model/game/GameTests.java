package be.pxl.crapapp.model.game;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import be.pxl.crapapp.exception.WrongPlayerException;
import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.model.User;

public class GameTests {
	private Game game;
	private User user1;
	private User user2;
	private List<Ship> ships;

	@Before
	public void init() {
		user1 = new User("TEST1", "TEST1", "TEST1", "TEST1", "TEST1", new ProfilePicture());
		user2 = new User("TEST2", "TEST2", "TEST2", "TEST2", "TEST2", new ProfilePicture());
		game = new Game(user1, user2, false);

		List<GameBoardSquare> carrierLocation = new ArrayList<>();
		carrierLocation.add(new GameBoardSquare('A', 1));
		carrierLocation.add(new GameBoardSquare('A', 2));
		carrierLocation.add(new GameBoardSquare('A', 3));
		carrierLocation.add(new GameBoardSquare('A', 4));
		carrierLocation.add(new GameBoardSquare('A', 5));

		List<GameBoardSquare> battleshipLocation = new ArrayList<>();
		battleshipLocation.add(new GameBoardSquare('B', 1));
		battleshipLocation.add(new GameBoardSquare('B', 2));
		battleshipLocation.add(new GameBoardSquare('B', 3));
		battleshipLocation.add(new GameBoardSquare('B', 4));

		List<GameBoardSquare> cruiserLocation = new ArrayList<>();
		cruiserLocation.add(new GameBoardSquare('C', 1));
		cruiserLocation.add(new GameBoardSquare('C', 2));
		cruiserLocation.add(new GameBoardSquare('C', 3));

		List<GameBoardSquare> submarineLocation = new ArrayList<>();
		submarineLocation.add(new GameBoardSquare('D', 1));
		submarineLocation.add(new GameBoardSquare('D', 2));
		submarineLocation.add(new GameBoardSquare('D', 3));

		List<GameBoardSquare> destroyerLocation = new ArrayList<>();
		destroyerLocation.add(new GameBoardSquare('E', 1));
		destroyerLocation.add(new GameBoardSquare('E', 2));

		ships = new ArrayList<>();
		ships.add(new CarrierShip(carrierLocation));
		ships.add(new BattleShip(battleshipLocation));
		ships.add(new CruiserShip(cruiserLocation));
		ships.add(new SubmarineShip(submarineLocation));
		ships.add(new DestroyerShip(destroyerLocation));
	}

	@Test
	public void constructorTest() {
		User user1 = new User();
		User user2 = new User();
		Game game = new Game(user1, user2, false);
		assertNotNull(game);
	}

	@Test
	public void placeShipsValidPlayer1Test() {
		game.placeShips(user1, ships);
	}

	@Test
	public void placeShipsValidPlayer2Test() {
		game.placeShips(user2, ships);
	}

	@Test(expected = WrongPlayerException.class)
	public void placeShipsInvalidPlayerTest() {
		game.placeShips(new User(), ships);
	}

	@Test
	public void validMoveHitPlayer1Test() {
		game.placeShips(user2, ships);
		game.move(user1, new GameBoardSquare('A', 1));
	}

	@Test
	public void validMoveMissPlayer2Test() {
		game.placeShips(user1, ships);
		game.move(user2, new GameBoardSquare('E', 5));
	}

	@Test(expected = WrongPlayerException.class)
	public void invalidMoveTest() {
		game.move(new User(), new GameBoardSquare('J', 10));
	}
}
