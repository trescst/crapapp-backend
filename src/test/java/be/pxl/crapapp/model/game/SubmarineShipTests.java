package be.pxl.crapapp.model.game;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SubmarineShipTests {
	@Test
	public void ConstructorTest() {
		List<GameBoardSquare> squares = new ArrayList<>();
		squares.add(new GameBoardSquare('A', 1));
		squares.add(new GameBoardSquare('B', 1));
		squares.add(new GameBoardSquare('C', 1));
		SubmarineShip ship = new SubmarineShip(squares);
		assertNotNull(ship);
	}
}
