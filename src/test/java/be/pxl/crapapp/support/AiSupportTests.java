package be.pxl.crapapp.support;

import static org.junit.Assert.assertNotEquals;

import java.util.List;

import org.junit.Test;
import org.junit.Test.None;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import be.pxl.crapapp.model.game.GameBoardSquare;
import be.pxl.crapapp.model.game.Ship;

@RunWith(SpringJUnit4ClassRunner.class)
public class AiSupportTests {
	@Test(expected=None.class)
	public void generateShipPlacementTest() {
		AiSupport.getShipPlacement();
	}
	
	@Test
	public void generateShipPlacementRandomnessTest() {
		List<Ship> ships1 = AiSupport.getShipPlacement();
		List<Ship> ships2 = AiSupport.getShipPlacement();
		assertNotEquals(ships1, ships2);
	}
	
	@Test
	public void randomSquareGenerationTest() {
		GameBoardSquare square1 = AiSupport.randomSquare();
		GameBoardSquare square2 = AiSupport.randomSquare();
	}
}
