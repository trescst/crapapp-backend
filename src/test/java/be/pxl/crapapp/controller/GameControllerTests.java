package be.pxl.crapapp.controller;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import be.pxl.crapapp.jwt.JWTAuthenticationFilter;
import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.model.game.Game;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.GameService;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.support.AiSupport;

@RunWith(SpringRunner.class)
@WebMvcTest(value = GameController.class)
public class GameControllerTests {
	@MockBean
	private GameService gameService;

	@MockBean
	private UserService userService;

	@MockBean
	private ProfilePictureService profilePictureService;

	@MockBean
	private BugService bugService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	private String URI = "/api/game/";

	@Test
	public void createSinglePlayerGameTest() throws Exception {
		User user = new User();

		Mockito.when(userService.getById(any(String.class))).thenReturn(user);
		Mockito.when(userService.getByUsername(any(String.class))).thenReturn(user);
		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("TEST", false));

		String token = JWT.create().withSubject("emailaddress").withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).header("Authorization", "Bearer " + token);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(201, response.getStatus());
	}

/*	@Test
	public void placeValidShipsTest() throws Exception {
		List<Object> carrierLocation = new ArrayList<>();
		Map<String,Object> cL1 = new HashMap<>();
		cL1.put("columnValue", 'A');
		cL1.put("row", 1);
		Map<String,Object> cL2 = new HashMap<>();
		cL2.put("columnValue", 'A');
		cL2.put("row", 2);
		Map<String,Object> cL3 = new HashMap<>();
		cL3.put("columnValue", 'A');
		cL3.put("row", 3);
		Map<String,Object> cL4 = new HashMap<>();
		cL4.put("columnValue", 'A');
		cL4.put("row", 4);
		Map<String,Object> cL5 = new HashMap<>();
		cL5.put("columnValue", 'A');
		cL5.put("row", 5);
		carrierLocation.add(cL1);
		carrierLocation.add(cL2);
		carrierLocation.add(cL3);
		carrierLocation.add(cL4);
		carrierLocation.add(cL5);
		Map<String, Object> carrier = new HashMap<>();
		carrier.put("type", "carrier");
		carrier.put("active", carrierLocation);
		carrier.put("sunk", false);
		carrier.put("id", 0);

		List<Object> battleshipLocation = new ArrayList<>();
		Map<String,Object> bL1 = new HashMap<>();
		bL1.put("columnValue", 'B');
		bL1.put("row", 1);
		Map<String,Object> bL2 = new HashMap<>();
		bL2.put("columnValue", 'B');
		bL2.put("row", 2);
		Map<String,Object> bL3 = new HashMap<>();
		bL3.put("columnValue", 'B');
		bL3.put("row", 3);
		Map<String,Object> bL4 = new HashMap<>();
		bL4.put("columnValue", 'B');
		bL4.put("row", 4);
		battleshipLocation.add(bL1);
		battleshipLocation.add(bL2);
		battleshipLocation.add(bL3);
		battleshipLocation.add(bL4);
		Map<String, Object> battleship = new HashMap<>();
		battleship.put("type", "battleship");
		battleship.put("active", battleshipLocation);
		battleship.put("sunk", false);
		battleship.put("id", 0);

		
		List<Object> cruiserLocation = new ArrayList<>();
		Map<String,Object> crL1 = new HashMap<>();
		crL1.put("columnValue", 'C');
		crL1.put("row", 1);
		Map<String,Object> crL2 = new HashMap<>();
		crL2.put("columnValue", 'C');
		crL2.put("row", 2);
		Map<String,Object> crL3 = new HashMap<>();
		crL3.put("columnValue", 'C');
		crL3.put("row", 3);
		cruiserLocation.add(crL1);
		cruiserLocation.add(crL2);
		cruiserLocation.add(crL3);
		Map<String, Object> cruiser = new HashMap<>();
		cruiser.put("type", "cruiser");
		cruiser.put("active", cruiserLocation);
		cruiser.put("sunk", false);
		cruiser.put("id", 0);

		List<Object> submarineLocation = new ArrayList<>();
		Map<String,Object> sL1 = new HashMap<>();
		sL1.put("columnValue", 'D');
		sL1.put("row", 1);
		Map<String,Object> sL2 = new HashMap<>();
		sL2.put("columnValue", 'D');
		sL2.put("row", 2);
		Map<String,Object> sL3 = new HashMap<>();
		sL3.put("columnValue", 'D');
		sL3.put("row", 3);
		submarineLocation.add(sL1);
		submarineLocation.add(sL2);
		submarineLocation.add(sL3);
		Map<String, Object> submarine = new HashMap<>();
		submarine.put("type", "submarine");
		submarine.put("active", submarineLocation);
		submarine.put("sunk", false);
		submarine.put("id", 0);

		List<Object> destroyerLocation = new ArrayList<>();
		Map<String,Object> dL1 = new HashMap<>();
		dL1.put("EolumnValue", 'E');
		dL1.put("row", 1);
		Map<String,Object> dL2 = new HashMap<>();
		dL2.put("EolumnValue", 'E');
		dL2.put("row", 2);
		destroyerLocation.add(dL1);
		destroyerLocation.add(dL2);
		Map<String, Object> destroyer = new HashMap<>();
		destroyer.put("type", "destroyer");
		destroyer.put("active", destroyerLocation);
		destroyer.put("sunk", false);
		destroyer.put("id", 0);
		
		List<Object> ships = new ArrayList<>();
		ships.add(carrier);
		ships.add(battleship);
		ships.add(cruiser);
		ships.add(submarine);
		ships.add(destroyer);

		User user1 = new User("1", "1", "1", "1", "USER", new ProfilePicture());
		User user2 = new User("2", "2", "2", "2", "USER", new ProfilePicture());
		Game game = new Game(user1, user2, false);

		Mockito.when(userService.getById(user1.getEmailAddress())).thenReturn(user1);
		Mockito.when(userService.getByUsername(user2.getUsername())).thenReturn(user2);
		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("TEST", false));
		Mockito.when(gameService.getGameById(any(Long.class))).thenReturn(game);

		String token = JWT.create().withSubject(user1.getEmailAddress()).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "1")
				.header("Authorization", "Bearer " + token).content(objectMapper.writeValueAsString(ships))
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}*/
	
	@Test
	public void moveTest() throws Exception {
		User user1 = new User("1", "1", "1", "1", "USER", new ProfilePicture());
		User user2 = new User("2", "2", "2", "2", "USER", new ProfilePicture());
		Game game = new Game(user1, user2, true);
		game.placeShips(user1, AiSupport.getShipPlacement());
		game.placeShips(user2, AiSupport.getShipPlacement());
		
		Map<String,Object> json = new HashMap<>();
		json.put("rowValue", 1);
		json.put("columnValue", "A");
		 
		Mockito.when(userService.getById(user1.getEmailAddress())).thenReturn(user1);
		Mockito.when(userService.getByUsername(any(String.class))).thenReturn(user2);
		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("TEST", false));
		Mockito.when(gameService.getGameById(any(Long.class))).thenReturn(game);
		
		String token = JWT.create().withSubject(user1.getEmailAddress()).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "1" + "/move")
				.header("Authorization", "Bearer " + token).content(objectMapper.writeValueAsString(json))
				.contentType(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}
}
