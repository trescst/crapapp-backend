package be.pxl.crapapp.controller;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import be.pxl.crapapp.jwt.JWTAuthenticationFilter;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.EmailService;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AdminController.class)
public class AdminControllerTests {
	@MockBean
	private UserService userService;
	
	@MockBean
	private EmailService mailService;
	
	@MockBean
	private ProfilePictureService profilePictureService;
	
	@MockBean
	private BugService bugService;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	private String URI = "/api/user/";
	private User testUser = new User();
	
	@Test
	public void getAllUsersTest() throws Exception {
		List<User> users = new ArrayList<User>();
		users.add(new User());
		users.add(new User());

		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		Mockito.when(userService.getAllUsers()).thenReturn(users);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).header("Authorization", "Bearer " + token)
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result;
		result = mockMvc.perform(requestBuilder).andReturn();
		String expectedJson = objectMapper.writeValueAsString(users);
		String outputJson = result.getResponse().getContentAsString();
		assertEquals(expectedJson, outputJson);
	}
	
	@Test
	public void validSearchTest() throws Exception {
		String URI = this.URI + "search";
		String searchParameter = "Java";
		Map<String,Object> rep = new HashMap<>();
		rep.put("searchParameter", searchParameter);
		
		List<User> users = new ArrayList<>();
		users.add(testUser);
		
		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
		
		Mockito.when(userService.usernameSearch(searchParameter)).thenReturn(users);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(rep)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(200, response.getStatus());
		assertEquals(objectMapper.writeValueAsString(users), response.getContentAsString());
	}
	
	@Test
	public void invalidSearchTest() throws Exception {
		String URI = this.URI + "search";
		String searchParameter = "=123?";
		Map<String,Object> rep = new HashMap<>();
		rep.put("searchParameter", searchParameter);
		
		List<User> users = new ArrayList<>();
		users.add(testUser);
		
		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
		
		Mockito.when(userService.usernameSearch(searchParameter)).thenReturn(users);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(rep)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(400, response.getStatus());
	}
	@Test
	public void noResultsSearchTest() throws Exception {
		String URI = this.URI + "search";
		String searchParameter = "empty";
		Map<String,Object> rep = new HashMap<>();
		rep.put("searchParameter", searchParameter);
		
		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(rep)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void blacklistUsersTest() throws Exception {
		String URI = this.URI + "blacklist";
		testUser = new User("test","","","","",null);
		User[] users = { testUser };
	
		Mockito.when(userService.getById(any(String.class))).thenReturn(testUser);
		
		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
		
		System.out.println(objectMapper.writeValueAsString(users));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(users)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}
	
	@Test
	public void unBlacklistUsersTest() throws Exception {
		String URI = this.URI + "blacklist/undo";
		testUser = new User("test","","","","",null);
		User[] users = { testUser };
	
		Mockito.when(userService.getById(any(String.class))).thenReturn(testUser);
		
		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(users)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}
	
	
}
