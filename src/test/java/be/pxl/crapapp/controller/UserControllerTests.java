package be.pxl.crapapp.controller;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import be.pxl.crapapp.jwt.JWTAuthenticationFilter;
import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.model.Token;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.EmailService;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.TokenService;
import be.pxl.crapapp.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class)
public class UserControllerTests {
	private HashMap<String, Object> userRepresentation;

	private User dary = new User();

	private String password = "Test123!";

	private String username = "HaatAanJavascript";

	private String emailAddress = "BeneIsDeLeukste@gmail.com";

	private String gender = "Female";

	private String URI = "/api/user/";

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private ProfilePictureService profilePictureService;

	@MockBean
	private UserService userService;

	@MockBean
	private EmailService emailService;

	@MockBean
	private TokenService tokenService;
	
	@MockBean
	private BugService bugService;

	@Before
	public void init() {
		dary.setUsername(username);
		dary.setPassword(password);

		userRepresentation = new HashMap<>();
		userRepresentation.put("username", username);
		userRepresentation.put("password", password);
		userRepresentation.put("email", emailAddress);
		userRepresentation.put("gender", gender);
	}

	@Test
	public void loginAcceptedTest() throws Exception {
		dary.setConfirmed(true);
		dary.setEnabled(true);

		Mockito.when(userService.getByUsername(username)).thenReturn(dary);
		Mockito.when(bugService.getBugByCode("BB_LP_WP")).thenReturn(new Bug("BB_LP_WP", false));
		
		Map<String, Object> request = new HashMap<>();
		request.put("username", username);
		request.put("password", password);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "login")
				.content(objectMapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		int statuscode = result.getResponse().getStatus();
		assertEquals(202, statuscode);
	}
	
	@Test
	public void loginAcceptedWrongPasswordBugTest() throws Exception {
		dary.setConfirmed(true);
		dary.setEnabled(true);

		Mockito.when(userService.getByUsername(username)).thenReturn(dary);
		Mockito.when(bugService.getBugByCode("BB_LP_WP")).thenReturn(new Bug("BB_LP_WP", true));
		
		Map<String, Object> request = new HashMap<>();
		request.put("username", username);
		request.put("password", "random value, it does not matter");

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "login")
				.content(objectMapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		int statuscode = result.getResponse().getStatus();
		assertEquals(202, statuscode);
	}

	@Test
	public void loginNotEnabledTest() throws Exception {
		dary.setConfirmed(true);

		Mockito.when(userService.getByUsername(username)).thenReturn(dary);

		Map<String, Object> request = new HashMap<>();
		request.put("username", username);
		request.put("password", password);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "login")
				.content(objectMapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		int statuscode = result.getResponse().getStatus();
		assertEquals(403, statuscode);
	}

	@Test
	public void loginNotConfirmedTest() throws Exception {
		dary.setEnabled(true);

		Mockito.when(userService.getByUsername(username)).thenReturn(dary);

		Map<String, Object> request = new HashMap<>();
		request.put("username", username);
		request.put("password", password);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "login")
				.content(objectMapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		int statuscode = result.getResponse().getStatus();
		assertEquals(403, statuscode);
	}

	@Test
	public void loginPasswordErrorTest() throws Exception {
		dary.setConfirmed(true);
		dary.setEnabled(true);

		String wrongPassword = "HelemaalFout123!";

		Mockito.when(bugService.getBugByCode("BB_LP_WP")).thenReturn(new Bug("BB_LP_WP", false));
		Mockito.when(userService.getByUsername(username)).thenReturn(dary);

		Map<String, Object> request = new HashMap<>();
		request.put("username", username);
		request.put("password", wrongPassword);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "login")
				.content(objectMapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		int statuscode = result.getResponse().getStatus();
		assertEquals(401, statuscode);
	}

	@Test
	public void loginWithNonExistingUserTest() throws Exception {
		String wrongUsername = "bene";
		String password = "unimportant";

		Map<String, Object> request = new HashMap<>();
		request.put("username", wrongUsername);
		request.put("password", password);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI + "login")
				.content(objectMapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		int statuscode = result.getResponse().getStatus();
		assertEquals(404, statuscode);
	}

	@Test
	public void registerCreatedTest() throws Exception {
		ProfilePicture profilePicture = new ProfilePicture();
		
		Mockito.when(profilePictureService.getProfilePictureById(1)).thenReturn(profilePicture);
		
		User testUser = new User(emailAddress, username, password, gender, "USER", profilePicture);

		userRepresentation.put("confirmPassword", password);

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		assertEquals(201, response.getStatus());
		assertEquals(objectMapper.writeValueAsString(testUser), response.getContentAsString());

	}

	@Test
	public void registerWrongMailPatternTest() throws Exception {
		String emailAddress = "BeneIsDeLeukste@gmail@com";

		userRepresentation.put("email", emailAddress);
		userRepresentation.put("confirmPassword", password);

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}
	
	@Test
	public void usernameLengthInvalidTest() throws Exception {
		userRepresentation.put("username", "ThisShouldProbablyAndWillLikelyAndActuallyInFactShouldFail");
		userRepresentation.put("confirmPassword", password);
		
		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}
	
	@Test
	public void usernameTooShortTest() throws Exception {
		userRepresentation.put("username", "1");
		userRepresentation.put("confirmPassword", password);
		
		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}
	
	@Test
	public void usernameInvalidTest() throws Exception {
		userRepresentation.put("username", "bob=<");
		userRepresentation.put("confirmPassword", password);
		
		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}

	@Test
	public void registerWrongConfirmPasswordTest() throws Exception {
		userRepresentation.put("confirmPassword", "NietHetJuistWachtwoord");

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}
	
	@Test
	public void registerUsernameTooShortTest() throws Exception {
		userRepresentation.put("username", "Ni");

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}
	
	@Test
	public void registerUsernameWithSpecialCharactersFailsTest() throws Exception {
		userRepresentation.put("username", "hsdfbn<ze");

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}

	@Test
	public void registerWithAlreadyExistingEmailTest() throws Exception {
		userRepresentation.put("confirmPassword", password);

		Mockito.when(userService.getById(emailAddress)).thenReturn(new User());

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(412, response.getStatus());
	}

	@Test
	public void registerWithAlreadyExistingUsernameTest() throws Exception {
		userRepresentation.put("confirmPassword", password);

		Mockito.when(userService.getByUsername(username)).thenReturn(new User());

		String URI = this.URI + "register";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(409, response.getStatus());
	}

	@Test
	public void validateExistingTokenTest() throws Exception {
		Token token = Token.generateToken(dary);

		Mockito.when(tokenService.getTokenById(token.getToken())).thenReturn(token);

		String URI = this.URI + "validate/" + token.getToken();
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
		assertEquals(dary.getUsername(), response.getContentAsString());
	}

	@Test
	public void validateNonExistingTokenTest() throws Exception {
		Token token = Token.generateToken(dary);

		String URI = this.URI + "validate/" + token.getToken();
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}
	
	@Test
	public void validateExistingPasswordTokenTest() throws Exception {
		Token token = Token.generateToken(dary);

		Mockito.when(tokenService.getTokenById(token.getToken())).thenReturn(token);

		String URI = this.URI + "validate/password/" + token.getToken();
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}

	@Test
	public void validateNonExistingPasswordTokenTest() throws Exception {
		Token token = Token.generateToken(dary);

		String URI = this.URI + "validate/password/" + token.getToken();
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void updateUserWithoutNewPasswordTest() throws Exception {
		String profilePicId = "something something";
		String oldPassword = "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26";

		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		User testUser = new User(emailAddress, username, password, gender, "USER", new ProfilePicture());

		userRepresentation.put("profilePic", profilePicId);
		userRepresentation.put("oldPassword", oldPassword);

		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(testUser);
		Mockito.when(userService.getByUsername(username)).thenReturn(testUser);

		String URI = this.URI + "update";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(202, response.getStatus());
		assertEquals(objectMapper.writeValueAsString(testUser), response.getContentAsString());
	}

	@Test
	public void updateUserWithNewPasswordTest() throws Exception {
		String profilePicId = "something something";
		String oldPassword = "Passfnja&28";

		User testUser = new User(emailAddress, username, oldPassword, gender, "USER", new ProfilePicture());

		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		userRepresentation.put("profilePic", profilePicId);
		userRepresentation.put("oldPassword", oldPassword);

		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(testUser);
		Mockito.when(userService.getByUsername(username)).thenReturn(testUser);

		String URI = this.URI + "update";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(202, response.getStatus());
		assertEquals(objectMapper.writeValueAsString(testUser), response.getContentAsString());
	}

	@Test
	public void updateUserWithSamePasswordFailsTest() throws Exception {
		String profilePicId = "Something something";
		String oldPassword = password;

		
		User testUser = new User(emailAddress, username, oldPassword, gender, "USER", new ProfilePicture());

		userRepresentation.put("profilePic", profilePicId);
		userRepresentation.put("oldPassword", oldPassword);

		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(testUser);
		Mockito.when(userService.getByUsername(username)).thenReturn(testUser);

		String URI = this.URI + "update";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}

	@Test
	public void updateUserWithWrongPasswordFailsTest() throws Exception {
		String profilePicId = "Something something";
		String wrongOldPassword = "Password123$";

		User testUser = new User(emailAddress, username, password, gender, "USER", new ProfilePicture());

		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		userRepresentation.put("profilePic", profilePicId);
		userRepresentation.put("oldPassword", wrongOldPassword);


		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(testUser);
		Mockito.when(userService.getByUsername(username)).thenReturn(testUser);

		String URI = this.URI + "update";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(401, response.getStatus());
	}

	@Test
	public void updateUserWithUsernameConflictFailsTest() throws Exception {
		String profilePicId = "Something something";
		String oldPassword = "";

		User testUser = new User(emailAddress, username, password, gender, "USER", new ProfilePicture());

		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		userRepresentation.put("profilePic", profilePicId);
		userRepresentation.put("oldPassword", oldPassword);


		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(testUser);
		Mockito.when(userService.getByUsername(username)).thenReturn(new User());

		String URI = this.URI + "update";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(userRepresentation)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(409, response.getStatus());
	}

	@Test
	public void forgotPasswordMailValidTest() throws Exception {
		String URI = this.URI + "forgotpassword";

		Mockito.when(userService.getById(emailAddress)).thenReturn(dary);

		HashMap<String, Object> emailAddress = new HashMap<>();
		emailAddress.put("email", this.emailAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(emailAddress)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}

	@Test
	public void forgotPasswordMailInvalidEmailTest() throws Exception {
		String URI = this.URI + "forgotpassword";

		HashMap<String, Object> emailAddress = new HashMap<>();
		emailAddress.put("email", this.emailAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(emailAddress)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void forgotPasswordValidTokenTest() throws Exception {
		String newPassword = "newPassword";

		Token token = Token.generateToken(dary);

		Mockito.when(tokenService.getTokenById(token.getToken())).thenReturn(token);

		HashMap<String, Object> passwordConfirmMap = new HashMap<>();

		passwordConfirmMap.put("password", newPassword);
		passwordConfirmMap.put("confirmPassword", newPassword);

		String URI = this.URI + "forgotpassword/" + token.getToken();

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(passwordConfirmMap)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}

	@Test
	public void forgotPasswordInvalidTokenTest() throws Exception {
		String newPassword = "newPassword";

		Token token = Token.generateToken(dary);

		HashMap<String, Object> passwordConfirmMap = new HashMap<>();

		passwordConfirmMap.put("password", newPassword);
		passwordConfirmMap.put("confirmPassword", newPassword);

		String URI = this.URI + "forgotpassword/" + token.getToken();

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(passwordConfirmMap)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void forgotPasswordInvalidConfirmationPasswordTest() throws Exception {
		String newPassword = "newPassword";

		Token token = Token.generateToken(dary);

		Mockito.when(tokenService.getTokenById(token.getToken())).thenReturn(token);

		HashMap<String, Object> passwordConfirmMap = new HashMap<>();

		passwordConfirmMap.put("password", newPassword);
		passwordConfirmMap.put("confirmPassword", "notTheSame");

		String URI = this.URI + "forgotpassword/" + token.getToken();

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(passwordConfirmMap)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}

	@Test
	public void forgotPasswordNewPasswordIsSameAsOldPasswordFailsTest() throws Exception {
		Token token = Token.generateToken(dary);

		HashMap<String, Object> passwordConfirmMap = new HashMap<>();

		Mockito.when(tokenService.getTokenById(token.getToken())).thenReturn(token);

		passwordConfirmMap.put("password", password);
		passwordConfirmMap.put("confirmPassword", password);

		String URI = this.URI + "forgotpassword/" + token.getToken();

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(passwordConfirmMap)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(400, response.getStatus());
	}

	@Test
	public void forgotUsernameValidTest() throws Exception {
		Mockito.when(userService.getById(emailAddress)).thenReturn(dary);

		String URI = this.URI + "forgotusername";

		HashMap<String, Object> emailAddress = new HashMap<>();
		emailAddress.put("email", this.emailAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(emailAddress)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}

	@Test
	public void forgotUsernameInvalidTest() throws Exception {
		String URI = this.URI + "forgotusername";

		HashMap<String, Object> emailAddress = new HashMap<>();
		emailAddress.put("email", this.emailAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(emailAddress)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void validDeleteUser() throws Exception {
		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(dary);

		String URI = this.URI + "delete";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(URI).header("Authorization", "Bearer " + token);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
		assertEquals(false, dary.isEnabled());
	}

	@Test
	public void invalidDeleteUser() throws Exception {
		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));		String URI = this.URI + "delete";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(URI).header("Authorization", "Bearer " + token);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void validGetUser() throws Exception {
		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));

		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		Mockito.when(userService.getById(emailAddress)).thenReturn(dary);
		
		String URI = this.URI + "profile";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).header("Authorization", "Bearer " + token);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}

	@Test
	public void invalidGetUser() throws Exception {
		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));


		Mockito.when(bugService.getBugByCode(any(String.class))).thenReturn(new Bug("XX_XX_XX", false));
		
		String URI = this.URI + "profile";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).header("Authorization", "Bearer " + token);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}

	@Test
	public void validTokenRequestTest() throws Exception {
		String URI = this.URI + "requestvalidation";

		Mockito.when(userService.getById(emailAddress)).thenReturn(dary);

		HashMap<String, Object> emailAddress = new HashMap<>();
		emailAddress.put("email", this.emailAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(emailAddress)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(200, response.getStatus());
	}

	@Test
	public void invalidTokenRequestTest() throws Exception {
		String URI = this.URI + "requestvalidation";

		HashMap<String, Object> emailAddress = new HashMap<>();
		emailAddress.put("email", this.emailAddress);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
				.content(objectMapper.writeValueAsString(emailAddress)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		assertEquals(404, response.getStatus());
	}
	
	@Test
	public void notfoundSearchTest() throws Exception {
		String URI = this.URI + "search";
		String searchParameter = "blobby";
		Map<String,Object> rep = new HashMap<>();
		rep.put("searchParameter", searchParameter);
		
		List<User> users = new ArrayList<>();
		users.add(dary);
		
		String token = JWT.create().withSubject("admin").withClaim("admin", true)
				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).header("Authorization", "Bearer " + token)
				.content(objectMapper.writeValueAsString(rep)).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		
		assertEquals(404, response.getStatus());
	}
	
//	@Test
//	public void uploadPicture() throws Exception {
//		String URI = this.URI + "uploadpicture";
//		MultipartFile file = new MockMultipartFile("file",  "some data".getBytes());
//		
//		String token = JWT.create().withSubject(emailAddress).withClaim("admin", false)
//				.withExpiresAt(new Date(System.currentTimeMillis() + JWTAuthenticationFilter.EXPIRATION_TIME))
//				.sign(HMAC512(JWTAuthenticationFilter.SECRET.getBytes()));
//
//		Mockito.when(userService.getById(emailAddress)).thenReturn(dary);
//	RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload(URI).header("Authorization", "Bearer " + token).content(file.getBytes()).contentType(MediaType.MULTIPART_FORM_DATA);
//		
//		
//		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//		MockHttpServletResponse response = result.getResponse();
//
//		
//		assertEquals(200, response.getStatus());
//	}
}
