package be.pxl.crapapp.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import be.pxl.crapapp.model.game.Game;
import be.pxl.crapapp.repository.GameRepository;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceImplementationTests {
	@Mock
	private GameRepository repository;
	
	@InjectMocks
	private GameServiceImplementation service;
	
	@Test
	public void addGameTest() {
		Game game = new Game();
		service.addGame(game);
		
		Mockito.verify(repository).saveAndFlush(game);
	}
	
	@Test
	public void updateBugTest() {
		Game game = new Game();
		service.addGame(game);
		
		Mockito.verify(repository).saveAndFlush(game);
	}
	
	@Test
	public void getGameByIdTest() {
		long id = 25135;
		service.getGameById(id);
		
		Mockito.verify(repository).findById(id);	
	}
}
