package be.pxl.crapapp.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.repository.BugRepository;

@RunWith(MockitoJUnitRunner.class)
public class BugServiceImplementationTests {
	@Mock
	private BugRepository repository;

	@InjectMocks
	private BugServiceImplementation service;

	@Test
	public void addBugTest() {
		Bug bug = new Bug();
		service.addBug(bug);
		
		Mockito.verify(repository).saveAndFlush(bug);
	}
	
	@Test
	public void updateBugTest() {
		Bug bug = new Bug();
		service.updateBug(bug);
		
		Mockito.verify(repository).saveAndFlush(bug);
	}
	
	@Test
	public void getAllBugsTest() {
		List<Bug> bugs = new ArrayList<>();
		bugs.add(new Bug("BTGABA", true));
		bugs.add(new Bug("BTGABF", false));
		
		Mockito.when(repository.findAll()).thenReturn(bugs);
		
		List<Bug> returnBugs =service.getAllBugs();
		
		Mockito.verify(repository).findAll();
		assertEquals(bugs, returnBugs);
	}
	
	@Test
	public void getBugByCodeTest() {
		String code = "BTGBBK";
		Bug bug = new Bug(code, true);
	
		Mockito.when(repository.findById(code)).thenReturn(Optional.of(bug));
		
		Bug returnBug = service.getBugByCode(code);
		
		Mockito.verify(repository).findById(code);
		assertEquals(bug, returnBug);
	}
	
	
}
