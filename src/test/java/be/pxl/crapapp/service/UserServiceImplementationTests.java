package be.pxl.crapapp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplementationTests {
	@Mock
	private UserRepository repository;
	
	@InjectMocks
	private UserServiceImplementation implementation;
	
	private User user;
	
	@Before
	public void init() {
		user = new User("test@test.test", "Test", "Test", "Test", "Test", new ProfilePicture());
	}
	
	@Test
	public void getByIdTest() {
		Mockito.when(repository.findById("test@test.test")).thenReturn(Optional.of(user));
		
		User returnUser = implementation.getById("test@test.test");
		
		Mockito.verify(repository).findById("test@test.test");
		assertEquals(user, returnUser);
	}
	
	@Test
	public void getAllTest() {
		List<User> users = new ArrayList<>();
		users.add(user);
		Mockito.when(repository.findAll()).thenReturn(users);
		
		List<User> returnUsers = implementation.getAllUsers();
		
		Mockito.verify(repository).findAll();
		assertEquals(users, returnUsers);
	}
	
	@Test
	public void updateUserTest() {
		implementation.updateUser(user);
		Mockito.verify(repository).saveAndFlush(user);
	}
	
	@Test
	public void findUserByUsernameTest() {
		Mockito.when(repository.findUserByUsername("Test")).thenReturn(user);
		
		User returnUser = implementation.getByUsername("Test");
		
		Mockito.verify(repository).findUserByUsername("Test");
		assertEquals(user, returnUser);
	}
	
	@Test
	public void searchOnUsernameTest() {
		List<User> users = new ArrayList<>();
		users.add(user);
		Mockito.when(repository.findByUsername("es")).thenReturn(users);
		
		List<User> returnUsers = implementation.usernameSearch("es");
		
		Mockito.verify(repository).findByUsername("es");
		assertEquals(users, returnUsers);
	}
	

	@Test
	public void searchOnUsernameNoResultsTest() {
		List<User> returnUsers = implementation.usernameSearch("es");
		
		Mockito.verify(repository).findByUsername("es");
		assertNull(returnUsers);
	}
	
	@Test
	public void getAllUsernamesTest() {
		List<String> names = new ArrayList<>();
		names.add("Test");
		Mockito.when(repository.getAllUsernames()).thenReturn(names);
		
		List<String> returnNames = implementation.getAllUsernames();
		
		Mockito.verify(repository).getAllUsernames();
		assertEquals(names, returnNames);
	}
	
	
}
