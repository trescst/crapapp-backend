package be.pxl.crapapp.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import be.pxl.crapapp.model.Token;
import be.pxl.crapapp.repository.TokenRepository;

@RunWith(MockitoJUnitRunner.class)
public class TokenServiceImplementationTests {
	@Mock
	private TokenRepository repository;

	@InjectMocks
	private TokenServiceImplementation service;
	
	@Test
	public void findTokenByIdTest() {
		Token expectedToken = new Token();
		Mockito.when(repository.findById(any(String.class))).thenReturn(Optional.of(expectedToken));
		
		Token returnToken = service.getTokenById("test");
		
		assertEquals(expectedToken, returnToken);
	}
	
	@Test
	public void deleteTokenTest() {
		Token token = new Token();
		
		service.deleteToken(token);
		
		Mockito.verify(repository).delete(token);
	}
	
	@Test
	public void addTokenTest() {
		Token token = new Token();
		Mockito.when(repository.saveAndFlush(token)).thenReturn(token);
		
		Token returnToken = service.addToken(token);
		
		assertEquals(token, returnToken);
		Mockito.verify(repository).saveAndFlush(token);
	}
}
