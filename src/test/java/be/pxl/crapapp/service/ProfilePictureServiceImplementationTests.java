package be.pxl.crapapp.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.repository.ProfilePictureRepository;

@RunWith(MockitoJUnitRunner.class)
public class ProfilePictureServiceImplementationTests {
	@Mock
	private ProfilePictureRepository repository;

	@InjectMocks
	private ProfilePictureServiceImplementation service;
	
	@Test
	public void profilePictureServiceTest() {
		ProfilePicture entity = new ProfilePicture();
		service.deleteProfilePicture(entity);
		Mockito.verify(repository).delete(entity);
	}
}
