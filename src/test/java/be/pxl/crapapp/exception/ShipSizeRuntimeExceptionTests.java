package be.pxl.crapapp.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class ShipSizeRuntimeExceptionTests {
	
	@Test
	public void EmptyConstructorTest() {
		ShipSizeRuntimeException exception = new ShipSizeRuntimeException();
		assertNotNull(exception);
	}
	
	@Test
	public void MessageConstructorTest() {
		String message = "Exception";
		ShipSizeRuntimeException exception = new ShipSizeRuntimeException(message);
		assertNotNull(exception);
		assertEquals(message, exception.getMessage());
	}
	
	@Test
	public void NestedExceptionConstructorTest() {
		ShipSizeRuntimeException exception = new ShipSizeRuntimeException(new ShipAmountRuntimeException());
		assertNotNull(exception);
		assertNotNull(exception.getCause());
	}
	
	@Test
	public void NestedExceptionWithMessageConstructorTest() {
		String message = "Exception";
		ShipSizeRuntimeException exception = new ShipSizeRuntimeException(message,new ShipAmountRuntimeException());
		assertNotNull(exception);
		assertNotNull(exception.getCause());
		assertEquals(message, exception.getMessage());
	}
}
